# Eigenen Laptop für die Verwendung von GIT vorbereiten
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie wissen wie man GIT auf einem Notebook installiert.<br>Sie wissen wie man einen universalen Quelltext-Editor auf einem Notebook installiert. |

## Installieren von Git

 - Laden Sie sich von https://git-scm.com/ *Git für Windows* in der aktuellsten Version herunter und installieren Sie es auf Ihrem persönlichen Notebook. Lassen Sie bei der Installation alles auf ***default***.

[Versionsverwaltung - Grundwissen..](
https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/02_Versionsverwaltung%20Grundwissen.md?ref_type=heads)

[Die wichtigsten Git-Befehle..](
https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/10_Git?ref_type=heads#die-wichtigsten-git-befehle)



# Account auf gitlab mit TBZ E-Mail Adresse erstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie wissen wie Sie einen persönlichen GitLab-Account erstellen<br>Sie wissen wie man einen SSH-Key generiert.<br>Sie wissen wie Sie einen SSH Key auf GitLab hinterlegen können. |

**Wichtig: Für diesen Arbeitsauftrag müssen Sie auf Ihrem Notebook Git for Windows installiert haben!**

## Anleitung
Eine ausführliche Anleitung zu SSH Keys erhalten Sie auf auf der GitLab Seite [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/). Die wichtigsten Schritte dabei sind: 

 - Erstellen Sie auf https://about.gitlab.com/ einen persönlichen GitLab-Account mit Ihrer TBZ E-Mail-Adresse. 
 - Öffnen Sie *Git Bash* und führen Sie folgenden Befehl aus, um Ihren persönlichen :
    ```bash
    ssh-keygen -t ed25519  -C "IHRE E-MAIL ADRESSE"
    ```
    Dann werden sie gefragt unter welchem namen sie den Private Key speichern wollen. Indem sie die Enter-Taste drücken akzeptieren sie den default:
    ```
    Enter file in which to save the key (/Users/ado/.ssh/id_ed25519):
    ```
    Jetzt können sie noch ein Passwort zum Verschlüsseln ihres Private Keys wählen. Wenn sie einfach die Enter-Taste drücken wird der Key nicht verschlüsselt:
    ```
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    ```
    Dann kommt der folgende output der ihnen sagt wo welche Keys gespeichert sind:
    ```
    Your identification has been saved in /Users/ado/.ssh/id_ed25519
    Your public key has been saved in /Users/ado/.ssh/id_ed25519.pub
    The key fingerprint is:
    SHA256:c8+o8U0Y9HAgl7iPg9wbYL2ZI/sTFjzXrwb4OYGB/vE ado@Armins-Air.tbz.local
    The key's randomart image is:
    +--[ED25519 256]--+
    |        ..o.     |
    |        .o..     |
    |       + .o..    |
    |      + B..+.    |
    |     + +S&o ..   |
    |      = /o+*  .  |
    |       =.Xo++.   |
    |      . ++Eoo    |
    |       .o..o.    |
    +----[SHA256]-----+
    ```

 - Geben Sie den öffentlichen Teil ihres Keys auf Ihrem Terminal aus. Geben sie folgendes Kommando im Terminal ein:
    ```bash
    cat .ssh/id_ed25519.pub
    ```
    Dann kommt ungefähr folgender output:
    ```
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFIrO0nZsJXi4qLRUD93f7YNNHVL8bvbZk3oDxSMN+6a ado@Armins-Air.tbz.local
    ```
    Diesen output können sie verwenden um im Gitlab den SSH-Key zu hinterlegen.
 - Hinterlegen Sie den Key in Ihrem Gitlab Account: Siehe https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account

 - Host-Key mit gitlab austauschen

    Git-Bash oeffnen und im terminal folgendes eingeben:
    ```
    git clone git@gitlab.com:ch-tbz-it/Stud/xxx.git
    ```
    Dann kommt folgendes
    ```
    Cloning into 'xxx'...
    The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
    ED25519 key fingerprint is SHA256:eUXGGm1YGsMAS7vkcx6JOJdOGHPem5gQp4taiCfCLB8.
    This key is not known by any other names
    Are you sure you want to continue connecting (yes/no/[fingerprint])? 
    ```
    Mit `yes` beantworten und den Rest der hier noch gezeigt ist ignorieren:
    ```
    Warning: Permanently added 'gitlab.com' (ED25519) to the list of known hosts.
    remote: 
    remote: ========================================================================
    remote: 
    remote: ERROR: The project you were looking for could not be found or you don't have permission to view it.

    remote: 
    remote: ========================================================================
    remote: 
    fatal: Could not read from remote repository.

    Please make sure you have the correct access rights
    and the repository exists.
    ```

- GIT-Config anpassen in Git-Bash:
    ```
    git config --global user.email "hans.muster@yzc.com"
    git config --global user.name "Hans Muster"
    ```
# Neues Repository für persönliches Portfolio erstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie wissen wie man ein Projekt auf gitlab erstellt und mit einem GIT Repository auf Ihrem lokalen PC verknüpft. |

Es gibt verschiedene Wege wie Sie ein neues Repository erstellen können. Grundsätzlich handelt es sich um einen Ordner auf Ihrem Computer den Sie mithilfe von GIT mit einem Server bzw. *Remote-Repository* synchronisieren können. Im Vergleich zu einem synchronisierten Ordner mit OneDrive, Google Drive, Dropbox oder ähnlich erfolgt die Synchronisierung nicht automatisch. 

## Anleitung - Remote first
Diese Anleitung eignet sich, wenn Sie noch kein lokales Repository erstellt haben und ein neues Repository (z.B. Ihr Portfolio) anlegen möchten. 
 1. Loggen Sie sich in Ihren gitlab Account ein. (https://gitlab.com/)
 2. Gehen Sie auf das Dashboard (Gitlab Startseite https://gitlab.com/)
 3. Klicken Sie auf *New Project*
 4. Wählen Sie *Create blank project*
 5. Geben Sie dem Projekt einen Selbsterklärenden Namen (z.B. "M231 Portfolio")
 6. Wählen *Visibility Level: Private* und lassen Sie die Checkbox bei *Initialize repository with a README* gesetzt. 
 7. Klicken Sie auf *Create Project*
 
Wenn Sie Visual Studio Code haben, machen Sie hier weiter:
https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md?ref_type=heads#wenn-sie-sich-mit-git-auskennen-d%C3%BCrfen-sie-auch-visual-studio-code-vcs-verwenden


 8. Kopieren Sie den Inhalt von entweder "**Clone with SSH**" oder "**Clone with HTTPS**" (Erklärung für welches an anderer Stelle) 

 9. Navigieren Sie auf der CMD oder dem PowerShell (mit cd .. usw.) an die Stelle, wo Sie Ihre Projekte haben werden. Normalerweise ist das Ihr "Workspace". Viele machen das in Windows unter `c:\users\{ihr-windows-account-name}\workspace`. 

Beachten Sie: 
<br>**Informatiker machen Projekte niemals auf dem "Desktop" von Windows oder Mac!!**

 10. Machen Sie in Ihrem "Workspace" dann den `git clone ...`-Befehl und wenn alles richtig eingestellt ist, wird GIT Ihr Projekt jetzt hierher kopieren.
```
git clone {hier der Clone-Link}
```


## Aufbau der GitLab Repository Startseite
![](images/gitlab_repo_overview.png)

## Repository Strukturierung
In der Strukturierung ihres Repositories sind sie grundsätzlich frei. Sie müssen jedoch folgendes beachten:
 - Der Aufbau und die Navigation müssen für den Leser intuitiv und ohne Anleitung bedienbar sein. 
 - Die Bewertungskriterien zum persönlichen Repository müssen erfüllt sein.

Als Vorlage für die Strukturierung ihres Repositories bzw. Portfolios können Sie [diese Vorlage](https://gitlab.com/alptbz/m231-portfolio) vorwenden. Passen Sie die Vorlage Ihren Bedürfnissen an. 

## Anleitung für GitHub
Auf [dieser Webseite](https://github.com/ser-cal/M300-Git-Repo-Setup#git-lokal-einrichten-und-ein-erstes-repository-initialisieren-und-synchronisieren) finden Sie eine Anleitung wie Sie ein neues Repository initialisieren können. 

## Anleitung -GitLab Project Member hinzufügen
![](images/gitlab_add_member.png)
Klicken Sie links in der Menüleiste auf *Project information* und anschliessend auf *Members*. 
![](images/gitlab_members.png)



## Wenn Sie sich mit Git auskennen, dürfen Sie auch Visual Studio Code VCS verwenden

Dies sollten Sie erst machen, wenn Sie sich gut auskennen mit Git auf dem command line interface (CLI), sonst werden Sie Schwierigkeiten haben, Git zu verstehen, weil VCS vieles alleine macht.

 - Laden Sie sich [Visual Studio Code](https://code.visualstudio.com/) herunter und installieren Sie es auf Ihrem persönlichen Notebook. Setzen Sie während der Installation die beiden Checkboxen "Add..." (siehe Screenshot):
![Visual Studio Installation](images/visualstudioinstallation_info.PNG)
 - Installieren Sie in Visual Studio Code die folgenden Plugins:
   - [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
   - [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
   - [Excel to Markdown table](https://marketplace.visualstudio.com/items?itemName=csholmq.excel-to-markdown-table)
   - [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
   - [German - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-german)
<br>
![Visual Studio Code Menü](images/visualstudiocodemenu.PNG)

## Tipps Visual Studio Code
 - Egal was sie machen möchten: Sie finden alles in der *Command pallete*. Der *default shortcut* ist ```Ctrl+Alt+P```<br>https://code.visualstudio.com/docs/getstarted/tips-and-tricks
 - **Markdown** (Plugins müssen installiert sein!):
   - Vorschau des aktuellen Markdown Dokumentes anzeigen:<br>1. ```Ctrl+Shift+P``` drücken<br>2. ```open preview``` tippen (Das ```>``` Symbol stehen lassen, sonst wird in den Dateisuchen-Modus gewechselt.) 3. Mit der Pfeiltaste den Eintrag *Markdown: Markdown Preview Enhanced: Open Preview to the side* auswählen und die *Enter*-Taste drücken.
   - Automatisch ein Inhaltsverzeichnis einfügen: *Command Pallete*: Der Befehl lautet *Markdown All in One: Create Table of Contents*
   - Automatisch Nummerieren: *Command Pallete*: *Markdown All in One: Add/Update section numbers*
   - Das Anfügen von ```<!-- omit in toc -->``` bewirkt, dass das entsprechende Heading nicht im Inhaltsverzeichnis aufgenommen wird und auch nicht nummeriert wird. 
![markdown omit in tok](images/markdown_omit.PNG)
 - Auf [Using Version Control in VS Code](https://code.visualstudio.com/docs/editor/versioncontrol) sind Erklärungen zu der *Source Control* Funktion von Visual Studio Code zu finden. Für Git müssen Sie keine zusätzlichen *Extensions* installieren.


## Grundlagen Installation Visual Studio Code & SSH-Keys
In dem nachfolgenden Video wird gezeigt, wie Sie einen...
- GitLab Account anlegen, 
- ein GitLab Projekt erstellen,
- Visual Studio Code und Git installieren,
- ein SSH Schlüsselpaar erstellen und den Public-Key ihrem GitLab Account hinzufügen,
- zusätzliche Markdown Plugins in VS Code installieren,
- das Template in ihrem Projekt einfügen,
- den ersten Commit erstellen und pushen,
- E-Mail und Benutzername in der Git Konfiguration für Ihren lokalen Benutzeraccount festlegen.

![Video zu VS Code Git Markdown GitLab](videos/CodeGitMarkdown.webm)

## Projekt klonen für Visual Studio Code

(Fortsetzung von https://gitlab.com/ch-tbz-it/Stud/m231/-/blob/master/10_Git/03_Eigene%20GIT%20Umgebung.md?ref_type=heads#anleitung-remote-first)

8. Klicken Sie auf *Clone* und wählen Sie bei *Open in your IDE* *Visual Studio Code (SSH)*
![Open in Visual Studio Code SSH](images/gitlab_open_in_visualstudio.PNG)
 9. Nach einem Bestätigungsdialog von Visual Studio Code müssen Sie ein Verzeichnis wählen, in welches das Repository geklont werden soll. Empfehlenswert ist es im *Homefolder* einen Ordner *source* zu erstellen und dann diesen Ordner zu wählen. 
 ```C:\Users\yourusername\source\```
 10. Visual Studio Code klont nun das Repository und öffnet es in einem neuen *Workspace*. (Voraussetzung: Der SSH-Key ist korrekt hinterlegt.)



## Video - Installation von Git & Visual Studio, Neues Projekt erstellen, Projekt klonen
Im nachfolgenden Video wird folgendes gezeigt:
 - Installation von Visual Studio Code und Git
 - Erstellen eines Projektes auf GitLab
 - Klonen des Projektes auf den lokalen Computer

Im Video wird das Vorgehen bei zwei typischen Fehler gezeigt:
 - GitLab Host Key Verifikation
 - Hinterlegen der eigenen E-Mail und des Namens für die Signatur der Commits (**Hinterlegen Sie ihre eigenen Daten!**)

![Installation von Git & Visual Studio, Neues Projekt erstellen, Projekt klonen](videos/git_visual_studio_installation.webm)

# Anleitungen
## Git HTTPs durch SSH Connection String ersetzen
Im Video wird gezeigt, wie Sie mithilfe des Texteditors vim den Connection String ändern können. 

![Git - HTTPs durch SSH Connection String ersetzen](videos/Git_HTTPs_durch_SSH_Connection_String.webm)
