# Versionsverwaltung mit GIT
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Einarbeitung in neues Thema |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie wissen was *GIT* ist, wie es grundsätzlich funktioniert und weshalb es für Sie als Plattformentwickler ein wichtiges Tool ist.  |

## Aufgabenstellung
Erreichen Sie das Ziel des Arbeitsauftrages indem Sie sich in das Thema Versionsverwaltung und GIT einlesen. Beantworten Sie anschliessend die Verständnisfragen. 

## Versionsverwaltung in der Informatik
Versionsverwaltung ist heute in der Softwareentwicklung nicht mehr wegzudenken. Die manuelle Nachverfolgung von Änderungen und die mühsame Zusammenführung von Quellcode wurde früh als eine Aufgabe angesehen, die vom Computer erledigt werden soll. Einer der frühstens Versionsverwaltungssoftware für UNIX wurde 1972 in Bell Labs entwickelt. (In diesem Labor in der Nähe von New York wurden viele Durchbrüche in der Telekommunikationstechnik, Mathematik, Physik und Materialforschung erzielt.) 1982 folgte das *Revision Control System* (kurz RCS)[^1], gefolgt vom bereits heute noch bekannten *Concurrent Version Control* (kurz CVS). Bevor Linus Torvalds 2005 Git veröffentlichte dominierten andere Applikationen den Markt, wie Subversion, Perforce oder BitKeeper.

[^1]: <https://www.cs.purdue.edu/homes/trinkle/RCShome/>

Einige von diesen Systemen, insbesondere BitKeeper, würden sich für einen jungen Git User wie eine Zeitreise anfühlen. Versionskontrollsoftware vor Git funktionierte nach einem grundsätzlich anderen Paradigma. In einer Taxonomie[^2] von Eric Sink, Author von *Version Control by Example*, ist Git eine Versionskontrollsoftware der dritten Generation, während die meisten Vorgänger, die in den 1990er und 2000er populär waren, zur zweiten Generation angehören. Der wesentliche Unterschied besteht darin, dass die *2nd Generation* zentralisiert (centralized) und die *3rd generation version control systems* verteilt funktioniert.

[^2]: Einheitliches Verfahren oder Modell (Klassifikationsschema) mit dem Objekte nach bestimmen Kriterien klassifiziert werden.

Versionskontrolle ist nicht nur in der Softwareentwicklung praktisch. Applikationen aller Arten, wie Word, Excel, Photoshop, verfolgen die Änderungen des Benutzers und geben ihm die Möglichkeit diese Rückgängig zu machen. Filehosting-Dienste wie Dropbox, die es ermöglichen einen Ordner auf mehreren Geräten zu spiegeln müssen ebenfalls Versionskontrolle betreiben, indem Sie feststellen, wenn eine identische Datei auf zwei verschiedenen Geräten gleichzeitig verändert wurde.

In der Systemtechnik kommen mit Software-defined Infrastructure (kurz SDI oder) und *Infrastructure as Code* immer mehr komplexe und mächtige Konfigurationsdateien zum Einsatz. In einer einzigen Datei lassen sich ganze Netzwerktopologien oder Serverinfrastrukturen festlegen. Auch hier eignet sich Versionskontrollsoftware besonders gut, um Änderungen ohne grossen Aufwand nachverfolgen zu können und die Teamarbeit zu vereinfachen. Beispiele dafür findet man bei cloud-init[^3], vagrant oder docker[^4].

[^3]: <https://cloudinit.readthedocs.io/en/latest/>

[^4]: <https://docs.docker.com/compose/>

Das Ziel dieser Übung ist, dass Sie sich mit dem Thema Versionskontrolle auseinandersetzen und die Vorzüge kennenlernen. Wenn Sie mehr über die Geschichte der Versionskontrolle erfahren möchten, empfehle ich Ihnen diese beiden Links[^5]. Weitere interessante Informationen finden Sie hier[^6].

[^5]: <https://dev.to/thefern/history-of-version-control-systems-vcs-43ch> <https://twobithistory.org/2018/07/07/cvs.html>

[^6]: <https://bitbucket.org/product/de/version-control-software>

## 3.3. Was ist GIT?

GIT ist ein sogenanntes Versionskontrollsystem (VCS) und wurde Anfang 2005 von Linus Torvalds, dem Initiator des Linux-Kernels, entwickelt. Es erstaunt deshalb nicht, dass GIT konzeptionell ähnlich aufgebaut ist wie ein Linux-Filesystem.

Einen guten Einstieg in das Thema GIT erhalten Sie unter den folgenden Links:
 - https://github.com/ser-cal/M300-Git-Repo-Setup
 - https://dev.to/milu_franz/git-explained-the-basics-igc
 - https://hackernoon.com/understanding-git-fcffd87c15a3


## 3.4. Verständnisfragen
 - Was bedeutet die Abkürzung DVCS? 
 - Wie heisst das bekannteste DVCS System? 
 - Was ist der Unterschied zwischen einer zentralisierten und einer dezentralisierten Versionsverwaltung? 
 - Was bedeutet in diesem Zusammenhang die Abkürzung "MD"?
 - Weshalb ist "MD" in Zusammenspiel mit DVCS so nützlich?
 - Was ist wesentlich umfangreicher wie "MD" und wird sehr gerne für wissenschaftliche Arbeiten verwendet (z.B. Ihre zukünftige Bachelorarbeit an einer Hochschule)? 
 - Weshalb ist der Einsatz von docx Dateien mit dem bekanntesten DVCS System problematisch?
