# Schutzwürdigkeit von Daten im Internet <!-- omit in toc -->

Im [Kompetenzband](../00_evaluation/) finden Sie eine Übersicht über Handlungskompetenzen, die Sie im Modul 231 zu erwerben haben. Welche Kompetenzen jeweils in einem Themenblock behandelt werden, finden Sie jeweils im Abschnitt *Ziele*.

# Übungen 

## Einzelarbeit
 - [Leseauftrag - EDÖB - Datenschutz Informationsdossier](01_Leseauftrag.md)
 - [Datenschutz und Datensicherheit](04_Datenschutz%20Datensicherheit.md)
 - [Video zum Thema Datenschutz (SRF)](09_Video%20SRF.md)
 - [Checklisten des Datenschutzbeauftragten des Kantons Zürichs](06_Checklisten.md)
 - [Auskunft über eigene Personendaten verlangen](05_Auskunftsrecht.md)
 - [Gefahren im Internet](11_Gefahren%20im%20Internet.md)

## Gruppenarbeiten
 - [Herausforderungen in der digitalen Welt](03_Herausforderungen%20in%20der%20digitalen%20Welt.md)
 - [Wichtigste Begriffe klären](02_Begriffe%20kl%C3%A4ren.md)
 - [Nutzungsbestimmungen bekannter Plattformen prüfen](07_Nutzungsbestimmungen.md)
 - [Unser soziales Netzwerk](08_Unser%20soziales%20Netzwerk.md)
 - [Internet als Datenarchiv](10_Internet%20als%20Datenarchiv.md)
 - [Reisebüro Teil 1](13_Reisebuero%20Teil%201.md)

--- 

# Ziele gemäss (siehe [Kompetenzmatrix](../00_Kompetenzband/))
 - A1G: Kann schützenswerte Daten erkennen.
 - A1F: Wendet in seiner Umgebung den Schutz der Daten konsequent an und befolgt Regeln.
 - A1E: Kann unterschiedliche Regelungen (zBps DSG, DSGVO, Schulordnung) vergleichen und kritisch hinterfragen.
 - F1G: Kann die Problematik bei unsicher konfigurierten Anwendungen erläutern.
 - F2G: Kennt die Bedeutung der AGBs und Datenschutzrichtlinien von Anbietern.
 - F2F: Kann die Datenschutzrichtlinie bezüglich Datenspeicherung überprüfen.

# Quellen
 - https://www.srf.ch/sendungen/myschool/datenschutz-2
 - https://www.digitale-gesellschaft.ch/auskunftsbegehren/
 - https://www.fedlex.admin.ch/eli/cc/1993/1945_1945_1945/de
 - https://www.economiesuisse.ch/de/artikel/datenschutz-eine-uebersicht-zum-neuen-gesetz
 - https://www.bj.admin.ch/dam/bj/de/data/staat/gesetzgebung/datenschutzstaerkung/vorentw-d.pdf.download.pdf/vorentw-d.pdf
 - https://www.bj.admin.ch/bj/de/home/staat/gesetzgebung/datenschutzstaerkung.html
 - https://www.kmu.admin.ch/kmu/de/home/aktuell/news/2021/neues-datenschutzgesetz-kmu-muessen-sich-anpassen.html
 - https://www.economiesuisse.ch/de/artikel/datenschutz-eine-uebersicht-zum-neuen-gesetz
 - https://www.economiesuisse.ch/de/artikel/datenschutzgesetz-bereiten-sie-sich-jetzt-vor
 - https://www.economiesuisse.ch/de/datenwirtschaft
 - https://www.rosenthal.ch/downloads/Rosenthal-Webinar-DSG-GDPR.pdf
 - https://www.myright.ch/de/business/rechtstipps/datenschutz/dsg-neu
 - https://www.edoeb.admin.ch/edoeb/de/home/datenschutz.html
 - https://www.stadt-zuerich.ch/pd/de/index/stadtpolizei_zuerich/praevention/digitale-medien/Gefahren/falsche-identitaeten.html
 - https://www.verbraucherzentrale.de/wissen/digitale-welt/datenschutz/welche-folgen-identitaetsdiebstahl-im-internet-haben-kann-17750
 - https://github.com/ser-cal/M300-Git-Repo-Setup
 - [https://de.wikipedia.org/wiki/Richtlinie_(EU)_2019/790_(Urheberrecht_im_digitalen_Binnenmarkt)](https://de.wikipedia.org/wiki/Richtlinie_(EU)_2019/790_(Urheberrecht_im_digitalen_Binnenmarkt))
 - https://www.20min.ch/story/das-bedeutet-das-neue-eu-gesetz-fuer-deine-memes-755504826807
 - https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A02019L0790-20190517&qid=1629634471866
 - https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/verbraucherinnen-und-verbraucher_node.html
 - https://www.edoeb.admin.ch/edoeb/de/home/datenschutz/ueberblick/datenschutz.html