# Nutzungsbestimmungen bekannter Plattformen prüfen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit, Gruppenarbeit |
| Aufgabenstellung  | Recherche |
| Zeitbudget  |  1 Lektion |
| Ziel | Nutzungsbestimmungen von Sozialen Netzwerken finden. Jeder lernende weiss, welche die wichtigsten und am häufigsten beschrieben Punkte in solchen Nutzungsbestimmungen sind.  |

## Aufgabenstellung
 - Pro Gruppe sollen 3 bis 4 Personen eine Plattform recherchieren. Beispiel für Plattformen: Reddit, Twitter, Instagram, TikTok, Discord, Signal, Whats-App, ...)
 - Die Plattformen werden unter den Lernenden aufgeteilt.
 - Zu jeder Plattform sollen folgende Fragen beantwortet werden:
   - Wo finde ich die Datenschutz-/Nutzungsbestimmungen (URL)?
   - Welche Daten (z.B. Personendaten) werden gespeichert (Auflisten)?
   - Für was werden die Daten verwendet?
   - Werden die Daten weitergegeben? Wenn ja, an wen?
   - Darf der Dienst/Service/Plattform meine Bilder/Videos weiterverkaufen?
   - Wie können die Daten gelöscht werden?
 - Die Ergebnisse werden anschliessend auf einem gemeinsamen Powerpoint festgehalten. Pro Plattform 1 bis max. 3 Folien.
 - Jede Gruppe stellt ihre Folie kurz vor und macht ein kurzes persönliches Fazit.  

## Vorgehen am Beispiel Facebook (Meta)

![Beispiel Vorgehen](media/privacypolicyaufgabe.PNG)

Im Screenshot werden folgende Schritte gezeigt:
 - Privacy Policy der gewählten Plattform suchen (hier im Beispiel mithilfe von Google)
 - Privacy Policy studieren und besprechen: Was ist da alles drin? Wie werden die Inhalte präsentiert?
 - Aufgabenrelevante Ausschnitte suchen und zusammenstellen
 - Weitere interessante Ausschnitte suchen und zusammenstellen
 - Beispiellösung einer Aufgabe: Die Auflistung aller gesammelten Daten (Auf Deutsch, relevante Informationen hervorgehoben)
