# Unser Reisebüro - Teil 1

Reiseunternehmen – “Günstig und Bequem an den Strand»

Das Reiseunternehmen *Günstig Reisen AG* im Zürich Oberland bietet in einer kleinen Filiale mit drei Mitarbeitern persönlich zugeschnittene Reiseangebote an. Die Kunden haben die Möglichkeit sich vor Ort oder via E-Mail beraten zu lassen. Auf der Webseite des Reiseunternehmens finden sich allgemeine Informationen und Kontaktangaben. 

Im dargestellten Diagramm ist vereinfacht ein Ablauf eines Reise-Verkaufs mit anschliessender Reisedurchführung dargestellt. 

Unabhängig davon, ob die Beratung im "physischen" Reisebüro oder online stattfindet, spätestens bei der Buchung müssen wir Angaben über unsere Person und Wünsche machen. Im Falle des Reisebüros überlegen wir uns:

**Welche Daten muss das Reisebüro zu welchem Zeitpunkt mindestens in Erfahrung bringen, damit es einerseits die Reise dem Kunden verkaufen und während der Reise den Kunden bei Schwierigkeiten unterstützen kann?**

## Ziele der Übung

In unserer zivilisierten Gesellschaft müssen wir für den Bezug von Waren und Dienstleistungen immer wieder Angaben über unsere Person und unsere Wünsche preisgeben. Durch das Verständnis über die dahinterliegenden Geschäftsprozesse ist es möglich Vermutungen darüber anzustellen, welche Angaben tatsächlich für die erfolgreiche Durchführung eines Prozesses benötigt werden und welche nicht. Während der Kunde befremdlich darauf reagiert, wenn er bei einer Pizza-Bestellung aufgefordert wird sein Körpergewicht anzugeben, wird er sich höchstwahrscheinlich weniger dabei überlegen, wenn er beim Kauf eines Fluges sein Geschlecht angeben muss. Da die Airline den Preis vermutlich nicht abgängig vom Geschlecht macht, es auch keine "Nur-Männer" Flüge gibt und es für die Identifikation wenig Aussagekräftig ist, lässt sich darüber spekulieren, weshalb die Airline diese Angabe einfordert. 

In dieser Übung soll der/die Lernende sein Bewusstsein darüber schärfen, welche Personendaten für den jeweiligen Geschäftsprozess notwendig sind und welche nicht. 
Weiter soll er sich damit auseinandersetzen, wann Personendaten erfasst und wie lange diese gespeichert werden. 

**" Nur so viele persönliche Daten preisgeben wie für den Zweck nötig. "**



## Aufgabenstellung

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Gruppenarbeit |
| Aufgabenstellung  | Theoretische Übung |
| Zeitbudget  |  1 Lektion |



1. Bilden Sie ein Team von drei bis vier Personen. 
2. Spielen Sie das Szenario "Beratung und Verkauf einer Reise" durch. Teilen Sie die Rollen in der Gruppe wie folgt auf:
   1. Eine Person spielt den Verkaufsberater
   2. Eine Person spielt den Kunden
   3. Die restlichen Personen führen protokoll darüber, welche Angaben (Name, Adresse, Wünsche, usw.) zu welchem Zweck und zu welchem Zeitraum vom Kunden preisgegeben werden. 

*Der Verkaufsberater soll während dem Gespräch mithilfe von Online-Buchungsplattformen die gewünschte Reise recherchieren (z.B. https://www.skyscanner.ch/). Dies unterstützt ihn dabei, die richtigen Angaben beim Kunden einzuholen. Der Kunde muss selbstverständlich keine korrekten Personalangaben machen, sondern darf diese frei erfinden.*

4. Tauschen Sie die Rollen und spielen Sie das Szenario erneut durch. 
5. Beantworten Sie die Fragen unten. (Tipp: Kopieren Sie sich diese Markdown-Datei in ihr persönliches Repository und fügen Sie die Antworten gleich an der entsprechenden stelle ein.)

## Fragen 

Vervollständigen Sie die Tabelle:

| Arbeitsschritt | Benötigte Angaben des Kunden |
|---|---|
| Reisewunsch des Kunden  | - Destination<br>- Dauer<br>- ... |
| Angebot zusammenstellen | |
| Angebot dem Kunden unterbreiten | |
| Reiseunterlagen zusammenstellen, Rechnung erstellen | |
| Kunde bezahlt Rechnung | |
| Kunde führt Reise erfolgreich durch | |

---

Welche Angaben des Kunden müssen im gesamten Zeitraum der Durchführung (der Reise) verfügbar sein und weshalb?

*Beispielszenario: Der Kunde ruft während den Ferien an, weil ein Flug gestrichen wurde. Er benötigt einen Ersatzflug.*

---

Welche Angaben des Kunden müssen nach erfolgreicher Durchführung der Reise (der Kunde ist zurück aus den Ferien) weiterhin verfügbar sein und zu welchem Zweck?

---

Welche Angaben des Kunden müssen nach erfolgreicher Durchführung gelöscht werden und aus welchem Grund?

---

Weitere Überlegungen und Hinweise:
 - Welche Angaben muss das Reisebüro aufnehmen, damit es z.B. bei einer Airline einen Flug buchen kann (Geschlecht, Geburtsdatum, Nationalität, usw)?
 - Muss ein Reisebüro die Nationalität des Kunden kennen und zu welchem Zweck?

---

Überlegen Sie zu jeder erfassten Angabe folgendes:
 - Verfügbarkeit: Wann müssen die Daten verfügbar sein? Wie lange müssen die Daten verfügbar sein? Wie schnell müssen die Daten verfügbar sein?
 - Vertraulichkeit: Wie vertraulich sind die erfassten Daten? Welche Daten müssen besonders geschützt werden?
 - Integrität: Welche Folgen kann es haben, wenn Daten manipuliert werden?

Anwendungsfälle:
 - Beispielszenario "Flug gestrichen" (siehe oben): Der Kunde ruft an, welche Daten müssen wie schnell verfügbar sein?
 - Ein Mitarbeiter überschreibt ausversehen den Reiseplan einer Reise, die sich gerade in Durchführung befindet. Welche Folgen kann der Verlust dieser Angaben haben?
 - Weshalb muss das Reisebüro den Rechnungsbeleg nach Abschluss der Reise aufbewahren?

---

| Erfasste Angabe | Dauer Aufbewahrung  | Wie schnell Verfügbar | Vertrauchlichkeit |
|---|---|---|---|
| Reisedestination | Steht zur Nachvollziehbarkeit auf der Rechnung, Rechnung wir 10 Jahre aufbewahrt | Während der Reisedurchführung: Sofort, wenn Kunde anruft, danach, innerhalb einem Arbeitstag | Schützenswert, sofern Bezug zur Person vorhanden ist, d.h. Reiseplan ohne Personenangaben und Datum ist nicht in Bezug auf Datenschutz nicht schützenswert |
| Geschlecht | | | |
| usw. | | | |



# Ablaufdiagramm

![Ablauf](media/AblaufReisebuero.PNG)